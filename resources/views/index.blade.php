<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>AES Crypter</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">


    <!--Bootstrap-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <!-- Styles -->
    <style>

        .console-log {
            height: 100% !important;
            width: 100% !important;
        }

        html, body {
            background-color: #f9f9f9;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            margin: 0;
        }

        .full-height {
            min-height: 100vh;
            height: 100%;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 36px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>


<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ route('login') }}">Login</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}">Register</a>
                @endif
            @endauth
        </div>
    @endif

    <div class="content">
        <div class="title m-b-md">
            AES CRYPTER
        </div>

        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                @if (session('alert'))
                    <div style="width:100%;margin-left:auto;margin-right:auto;text-align:center;" id="alert"
                         name="alert" class="alert alert-dark bg-danger text-dark">
                        {{ session('alert') }}
                    </div>
                @elseif (session('status'))
                    <div style="width:100%;margin-left:auto;margin-right:auto;text-align:center;" id="alert"
                         name="alert" class="alert alert-dark bg-danger text-dark">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="card" style="min-width:400px;">

                    <div class="card-header bg-dark text-light">
                        <span> Calculator</span>
                    </div>
                    <div class="card-body">
                        <form action="{{route('main')}}" method="get">
                            @CSRF
                            <div class="form-group">
                                <label for="action_field">Choose the crypter mode</label>
                                <select id="action_field" name="action" class="custom-select">
                                    <option value="encrypt">Encrypt</option>
                                  {{--  <option value="decrypt">Decrypt</option> --}}
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="key">Key</label>
                                <input type="text" class="form-control" name="key" id="key"
                                       placeholder="Your key goes here">
                            </div>
                            <div class="form-group">
                                <select name="keyType" id="KeyType" class="custom-select">
                                    <option value="string">String</option>
                                    <option value="hex">Hexadecimal</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="message_field">Message</label>
                                <input type="text" class="form-control" name="message" id="message_field"
                                       placeholder="Your message goes here">
                                <select name="messageType" id="messageType" class="custom-select">
                                    <option value="string">String</option>
                                    <option value="hex">Hexadecimal</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <input class="btn btn-dark text-light btn-block" type="submit" value="Execute">
                            </div>
                        </form>
                    </div>
                    <br/>
                    <div class="container-fluid d-inline text-dark">
                        <div class="card">
                            <div class="card-body console-log">
                                <h2 style="font-size:22px;">Console:</h2>
                                <hr>
                                <br/>

                                <span id="result" style="text-decoration: underline;">
                                    @isset($finalHex)  <span style="font-weight:bold;">Encrypted message:</span> {{$finalHex}}  <br/>
                                    @else Nothing to see here... <br/>
                                    @endisset
                                    </span>
                                <hr>

                                @isset($roundKeys)
                                      <div class="info roundkeys">
                                          <span>Round keys:<br /></span>
                                          @foreach($roundKeys as $key=>$value)
                                              <span style="font-weight:bold;text-decoration: underline;">Round {{$loop->index+1}}:</span> {{$value}}<br />
                                          @endforeach
                                      </div>
                                    <br />
                                @endisset

                                @isset($subkeys)
                                     <div class="info subkeys">
                                         <span>Subkeys:<br /></span>
                                         @foreach($subkeys as $subkey)
                                             <span style="font-weight:bold;text-decoration: underline;">Round {{$loop->index+1}}:</span> {{$subkey}} <br />
                                         @endforeach
                                     </div>
                                    <br />
                                 @endisset

                                @isset($mixColumns)
                                    <div class="info mixcolumns">
                                        <span>After mixed columns:<br /></span>
                                        @foreach($mixColumns as $mixColumn)
                                            <span style="font-weight:bold;text-decoration: underline;">Round {{$loop->index+1}}:</span> {{$mixColumn}} <br />
                                        @endforeach
                                    </div>
                                    <br />
                                @endisset

                                 @isset($shiftedKeys)
                                    <div class="info shiftedkeys">
                                        <span>Shifted keys:<br /></span>
                                        @foreach($shiftedKeys as $shiftedKey)
                                            <span style="font-weight:bold;text-decoration: underline;">Round {{$loop->index+1}}:</span> {{$shiftedKey}} <br />
                                        @endforeach
                                    </div>
                                    <br />
                                @endisset

                                @isset($stateMatrix)
                                    <div class="info statematrix">
                                        <span>State Matrix keys:<br /></span>
                                        @foreach($stateMatrix as $MatrixKey)
                                           <span style="font-weight:bold;text-decoration: underline;">Round {{$loop->index+1}}:</span> {{$MatrixKey}} <br />
                                        @endforeach
                                    </div>
                                    <br />
                                @endisset
                            </div>
                        </div>
                    </div>
                    <div style='font-size:10px; margin-left:30px;'>&copy; 2019 VINCENTAS BAUBONIS</div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
