<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Operation extends Model
{


	private static $fixedMatrix=[
		['02','03','01','01'],
		['01','02','03','01'],
		['01','01','02','03'],
		['03','01','01','02'],
	];

	private static $sBox = [
		'0'=>['63','7c','77','7b','f2','6b','6f','c5','30','01','67','2b','fe','d7','ab','76'],
		'1'=>['ca','82','c9','7d','fa','59','47','f0','ad','d4','a2','af','9c','a4','72','c0'],
		'2'=>['b7','fd','93','26','36','3f','f7','cc','34','a5','e5','f1','71','d8','31','15'],
		'3'=>['04','c7','23','c3','18','96','05','9a','07','12','80','e2','eb','27','b2','75'],
		'4'=>['09','83','2c','1a','1b','6e','5a','a0','52','3b','d6','b3','29','e3','2f','84'],
		'5'=>['53','d1','00','ed','20','fc','b1','5b','6a','cb','be','39','4a','4c','58','cf'],
		'6'=>['d0','ef','aa','fb','43','4d','33','85','45','f9','02','7f','50','3c','9f','a8'],
		'7'=>['51','a3','40','8f','92','9d','38','f5','bc','b6','da','21','10','ff','f3','d2'],
		'8'=>['cd','0c','13','ec','5f','97','44','17','c4','a7','7e','3d','64','5d','19','73'],
		'9'=>['60','81','4f','dc','22','2a','90','88','46','ee','b8','14','de','5e','0b','db'],
		'a'=>['e0','32','3a','0a','49','06','24','5c','c2','d3','ac','62','91','95','e4','79'],
		'b'=>['e7','c8','37','6d','8d','d5','4e','a9','6c','56','f4','ea','65','7a','ae','08'],
		'c'=>['ba','78','25','2e','1c','a6','b4','c6','e8','dd','74','1f','4b','bd','8b','8a'],
		'd'=>['70','3e','b5','66','48','03','f6','0e','61','35','57','b9','86','c1','1d','9e'],
		'e'=>['e1','f8','98','11','69','d9','8e','94','9b','1e','87','e9','ce','55','28','df'],
		'f'=>['8c','a1','89','0d','bf','e6','42','68','41','99','2d','0f','b0','54','bb','16']
	];

	static function changeMatrixRepresentation(Array $matrix) : Array
	{
		$newMatrix = [];
		$newMatrix['0']=[];
		$newMatrix['1']=[];
		$newMatrix['2']=[];
		$newMatrix['3']=[];
		for($i = 0; $i < count($matrix); $i++){
			array_push($newMatrix['0'],$matrix[$i][0]);
			array_push($newMatrix['1'],$matrix[$i][1]);
			array_push($newMatrix['2'],$matrix[$i][2]);
			array_push($newMatrix['3'],$matrix[$i][3]);
		}

		return $newMatrix;
	}

	public static function splitToMatrix(String $value) : array
	{
		$matrix = [];
		$values = str_split($value,2);
		$matrix = array_chunk($values, 4);

		return $matrix;
	}


	public static function addRoundkey(Array $firstMatrix, Array $secondMatrix) : Array
	{
		$resultMatrix = [];
		for($i = 0; $i<count($firstMatrix); $i++){
			$temp = [];
			for($j = 0; $j<count($firstMatrix[$i]);$j++){
				$xorResult = self::calculateXOR(Conversion::hexToBinary($firstMatrix[$i][$j]),Conversion::hexToBinary($secondMatrix[$i][$j]));
				if(strlen(base_convert($xorResult, 2, 16)) < 2){
					array_push($temp,'0'.base_convert($xorResult, 2, 16));
				}else{
					array_push($temp,base_convert($xorResult, 2, 16));
				}
			}
			array_push($resultMatrix,$temp);
		}

		return $resultMatrix;
	}

	public static function calculateXOR(String $cKey,String $bitSel)  : String
	{
		$xor1 = gmp_init($cKey, 2);
		$xor2 = gmp_init($bitSel, 2);
		$xor3 = gmp_xor($xor1, $xor2);
		if(strlen(gmp_strval($xor3, 2)) < 8){
				$temp = gmp_strval($xor3, 2);
				while(strlen($temp) < 8){
					$temp = '0'.$temp;
				}
				return $temp;
		}

		return gmp_strval($xor3, 2);
	}

	public static function subBytes(Array $matrix) : Array
	{
		$newMatrix = [];
		for($i = 0; $i < count($matrix); $i++){
			$temp = [];
			for($j = 0; $j < count($matrix[$i]);$j++){
				$coords = str_split($matrix[$i][$j]);
				array_push($temp, self::$sBox[$coords[0]][hexdec($coords[1])]);
			}
			array_push($newMatrix, $temp);
		}

		return $newMatrix;
	}

	public static function shiftRow(Array $matrix) : Array
	{
		$newMatrix = [];
		$changedRepresentation = self::changeMatrixRepresentation($matrix);
		foreach ($changedRepresentation as $key => $column) {
			$i = 0;
			 while($i < $key){
			 	array_push($column,array_shift($column));
			 	$i++;
			 }
			 array_push($newMatrix, $column);
		}
		$newMatrix = self::changeMatrixRepresentation($newMatrix);

		return $newMatrix;
	}


	public static function mixColumn(Array $matrix) : Array
	{
		$newMatrix = [];
		$membersArray = [];

		for($i = 0; $i<count($matrix); $i++){
				$temp = [];
				for($k = 0; $k < count(self::$fixedMatrix);$k++){
					$xors = [];
					for($m=0; $m < count(self::$fixedMatrix[$k]);$m++){
						$multAgainst = hexdec(self::$fixedMatrix[$k][$m]);
						 if($multAgainst == '1'){
							$val = Conversion::hexToBinary($matrix[$i][$m]);
						} else if($multAgainst == '2'){
							$val = self::multiply(Conversion::hexToBinary($matrix[$i][$m]));
						}else {
							$val = self::multiply(Conversion::hexToBinary($matrix[$i][$m]));
							$val = self::calculateXOR($val, Conversion::hexToBinary($matrix[$i][$m]));
						}
						array_push($xors,$val);
					}
					$val = self::calculateXOR($xors[0],array_pop($xors));
					while(count($xors) > 1){
						$val = self::calculateXOR($val,array_pop($xors));
					}
					$val = base_convert($val, 2, 16);
					if(strlen($val) < 2){
						$val = str_split($val);
						array_unshift($val,'0');
						$val = implode('', $val);
					}
					array_push($temp,$val);
				}
				array_push($membersArray,$temp);
		}
		//var_dump($membersArray);

		return $membersArray;
	}



	public static function multiply(String $binary) : String
	{
		$binaryArray = str_split($binary);
		if($binaryArray[0] == 1){
			array_shift($binaryArray);
			array_push($binaryArray, '0');
			$binary = implode('', $binaryArray);
			return self::calculateXOR($binary,'00011011');
		}else{

			return decbin(bindec($binary)* 2);
		}

		return $binary;
	}

	public static function rotWord(Array $word) : Array
	{
		array_push($word,array_shift($word));

		return $word;
	}

	public static function subBoxKeySch(Array $word) : Array
	{
		$temp = [];
		for($i = 0; $i < count($word); $i++)
		{
			$coords = str_split($word[$i]);
			array_push($temp,self::$sBox[$coords[0]][hexdec($coords[1])]);
		}

		return $temp;
	}


	public static function keyShedule(Array $matrix) : Array
	{
		$roundKeys = $matrix;
		//var_dump($roundKeys);
		$rcons = [
			4=>'01',
			8=>'02',
			12=>'04',
			16=>'08',
			20=>'10',
			24=>'20',
			28=>'40',
			32=>'80',
			36=>'1b',
			40=>'36',
		];
		for ($i = 4; $i < 44; $i++ ){
			if($i % 4 == 0 ){
				$temp = [];
				$first = self::rotWord($roundKeys[$i-1]);
				$first = self::subBoxKeySch($first);
				$first[0] = base_convert(self::calculateXOR(Conversion::hexToBinary($first[0]),Conversion::hexToBinary($rcons[$i])), 2, 16);
				if(strlen($first[0]) < 2){
						$first[0] = '0'.$first[0];
					}
				for($j = 0; $j < count($first); $j++){
					$first[$j] = base_convert(self::calculateXOR(Conversion::hexToBinary($first[$j]), Conversion::hexToBinary($roundKeys[$i-4][$j])),2,16);
					if(strlen($first[$j]) < 2){
						$first[$j] = '0'.$first[$j];
					}
				}
				array_push($roundKeys, $first);
			}else{
				$first = [];
				for($j = 0; $j < count($roundKeys[$i-1]); $j++){
					$first[$j] = base_convert(self::calculateXOR(Conversion::hexToBinary($roundKeys[$i-1][$j]), Conversion::hexToBinary($roundKeys[$i-4][$j])),2,16);
					if(strlen($first[$j]) < 2){
						$first[$j] = '0'.$first[$j];
					}
				}
				array_push($roundKeys, $first);
			}
		}

		$allKeys = array_chunk($roundKeys, 4);
		return $allKeys;
	}



}
