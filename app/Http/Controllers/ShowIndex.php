<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;


class ShowIndex extends Controller
{
    /**
     * Show home index
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke()
    {
        return view('index');
    }
}
