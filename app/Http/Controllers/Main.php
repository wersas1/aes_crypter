<?php

namespace App\Http\Controllers;

use App\Conversion;
use App\Operation;
use Illuminate\Http\Request;

class Main extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function __invoke(Request $request)
    {

        if (isset( $request->key)) $key = $_GET['key'];
        else return redirect()->route("welcome")->with('status', 'Key not set');

        if (isset($request->message)) $message = $_GET['message'];
        else return redirect()->route("welcome")->with('status', 'Msg not set');
        if (isset($request->keyType)) {
        //128, 192, 256
            if ($_GET['keyType'] == 'hex') {
                $key = Conversion::adjustHex($key);
            } else {
                $key = Conversion::convertStringToHexVal($key);
                $key = Conversion::adjustHex($key);
            }
        } else return redirect()->route("welcome")->with('status', 'Key Type not set');


        if (isset($request->messageType)) {
            if ($_GET['messageType'] == 'hex') {
                $message = Conversion::adjustHex($message);
            } else {
                $message = Conversion::convertStringToHexVal($message);
                $message = Conversion::adjustHex($message);
            }
        } else return redirect()->route("welcome")->with('status', 'Msg Type not set');

        //Key is split to matrix
        $key = Operation::splitToMatrix($key);
        $message = Operation::splitToMatrix($message);

        // Calculate Round keys
        $roundKeys = Operation::keyShedule($key);
        $stateMatrix = [$message];

        $subkeys = [];
        $mixColumns = [];
        $shiftedKeys = [];
        foreach ($roundKeys as $key => $value) {
            $roundMatrix = Operation::addRoundkey($value, $stateMatrix[$key]);

            $subBytesMatrix = Operation::subBytes((array)$roundMatrix);
            array_push($subkeys, $subBytesMatrix);
            $shiftedMatrix = Operation::shiftRow((array)$subBytesMatrix);
            array_push($shiftedKeys, $shiftedMatrix);
            if ($key != count($roundKeys) - 2) {
                $mixedColumn = Operation::mixColumn((array)$shiftedMatrix);
                array_push($mixColumns, $mixedColumn);
                array_push($stateMatrix, $mixedColumn);
            } else {
                $result = Operation::addRoundkey($roundKeys[$key + 1], (array)$shiftedMatrix);
                $finalHex = [];
                foreach ($result as $key => $value) {
                    $value = implode('', $value);
                    array_push($finalHex, $value);
                }
                $finalHex = implode('', $finalHex);
                break;
            }
        }

        $roundKeysFinal = [];
        foreach ($roundKeys as $key => $value) {
            $val = $this->converter($value);
            $roundKeysFinal[$key] = $val;
        }

        $afterSBoxFinal=[];
        foreach ($subkeys as $key => $value) {
            $val = $this->converter($value);
            $afterSBoxFinal[$key] = $val;
        }

        $shiftedKeysFinal=[];
        foreach ($shiftedKeys as $key => $value) {
           $val = $this->converter($value);
           $shiftedKeysFinal[$key] = $val;
        }

        $mixColumnsFinal=[];
        foreach ($mixColumns as $key => $value) {
            $val = $this->converter($value);
            $mixColumnsFinal[$key] = $val;
        }

        $stateMatrixFinal=[];
        foreach ($stateMatrix as $key => $value) {
            $val = $this->converter($value);
            $stateMatrixFinal[$key] = $val;
        }

       // echo '<div id="keys"></div>';

       // echo "<script src='script.js'></script>";

        return view('index', [
            'finalHex' => $finalHex,
            'roundKeys' => $roundKeysFinal,
            'subKeys' => $afterSBoxFinal,
            'mixColumns' => $mixColumnsFinal,
            'shiftedKeys' => $shiftedKeysFinal,
            'stateMatrix' => $stateMatrixFinal,
        ]);

    }

    function converter(array $matrix)
    {
        $temp = [];
        foreach ($matrix as $key => $value) {
            array_push($temp, implode('', $value));
        }
        return implode('', $temp);
    }

}
