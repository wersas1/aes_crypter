<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Conversion extends Model
{
	private static $hexToBinDictionary =[
			  '0'=>'0000','1'=>'0001','2'=>'0010','3'=>'0011',
			  '4'=>'0100','5'=>'0101','6'=>'0110','7'=>'0111',
			  '8'=>'1000','9'=>'1001','A'=>'1010','B'=>'1011',
			  'C'=>'1100','D'=>'1101','E'=>'1110','F'=>'1111'
	];

	public static function convertStringToHexVal(String $val) : String{
		$val = bin2hex($val);

		return $val;
	}

	public static function adjustHex(String $key) : String{
		if(strlen($key) <= 32){
			while(strlen($key) !== 32){
				$key=$key.'0';
			}
		}else if(strlen($key) <= 48){
			while(strlen($key) !== 48){
				$key=$key.'0';
			}
		}else{
			while(strlen($key) < 64){
				$key=$key.'0';
			}
		}

		return $key;
	}

	public static function hexToBinary(String $value) : String{
		$splitKey = str_split(strtoupper($value));
		$binaryValue = [];
		foreach ($splitKey as $key => $char){
			array_push($binaryValue,self::$hexToBinDictionary[strtoupper($char)]);
		}
		$binaryValue = implode('', $binaryValue);
		$binaryValue = chunk_split($binaryValue, 8, ' ');

		return trim($binaryValue);
	}

}
